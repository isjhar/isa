/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.isaapp;

import com.shiddiq.entity.Node;
import com.shiddiq.util.ReadExcel;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author Isjhar-pc
 */
public class GASBitGeneTest extends TestCase {
    
    public GASBitGeneTest(String testName) {
        super(testName);
    }

    public void testGA1()
    {
        System.out.println("TEST CASE GA 1");
        List<Node> nodes = ReadExcel.ReadExcel("data\\superframe_scheduling.xls");
        int totalTime = 100;
        int timePerSlot = 10;
        GASBitGene gasBitGene = new GASBitGene(nodes, totalTime, timePerSlot);
        try 
        {
            gasBitGene.simulateScheduling();   
            gasBitGene.summaryResult();
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace(System.out);
        }
    }
}
