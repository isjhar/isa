/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.isaapp;

import com.shiddiq.entity.Node;
import com.shiddiq.util.ReadExcel;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 *
 * @author Isjhar-pc
 */
public class GASIntegerTest extends TestCase
{
    public GASIntegerTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( GASIntegerTest.class );
    }
    
    public void testGA1()
    {
        System.out.println("TEST CASE GA 1");
        List<Node> nodes = ReadExcel.ReadExcel("data\\superframe_scheduling.xls");
        int totalTime = 100;
        int timePerSlot = 10;
        GASIntegerGene gasInteger = new GASIntegerGene(nodes, totalTime, timePerSlot);
        try 
        {
            gasInteger.simulateScheduling();   
            gasInteger.summaryResult();
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace(System.out);
        }
    }
}
