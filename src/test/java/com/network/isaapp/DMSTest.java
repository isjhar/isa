package com.network.isaapp;

import com.shiddiq.util.ReadExcel;
import java.util.List;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import com.shiddiq.entity.Node;
import jdk.nashorn.internal.ir.annotations.Ignore;

/**
 * Unit test for simple App.
 */
public class DMSTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public DMSTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( DMSTest.class );
    }

    @Ignore
    public void testDMS1()
    {
        System.out.println("TEST CASE DMS 1");
        List<Node> nodes = ReadExcel.ReadExcel("data\\message_scheduling.xls");
        int totalTime = 500;
        int timePerSlot = 10;
        DMS dms = new DMS(nodes, totalTime, timePerSlot);
        try 
        {
            Node[] expectedExecutionOrder = new Node[totalTime / timePerSlot];
            dms.simulateScheduling();    
            dms.summaryResult();
            /// validate array length
            assertEquals(expectedExecutionOrder.length, dms.getResult().getExecutionOrder().length);
            
            /// create data
            expectedExecutionOrder[0] = nodes.get(0);
            expectedExecutionOrder[1] = nodes.get(1);
            expectedExecutionOrder[2] = nodes.get(1);
            expectedExecutionOrder[3] = nodes.get(2);
            expectedExecutionOrder[4] = nodes.get(4);
            expectedExecutionOrder[5] = nodes.get(2);
            expectedExecutionOrder[6] = nodes.get(3);
            expectedExecutionOrder[7] = nodes.get(0);
            expectedExecutionOrder[8] = nodes.get(3);
            expectedExecutionOrder[9] = nodes.get(4);
            
            expectedExecutionOrder[10] = nodes.get(2);
            expectedExecutionOrder[11] = nodes.get(2);
            expectedExecutionOrder[12] = nodes.get(3);
            expectedExecutionOrder[13] = nodes.get(3);
            expectedExecutionOrder[14] = nodes.get(0);
            expectedExecutionOrder[15] = nodes.get(4);
            expectedExecutionOrder[16] = nodes.get(1);
            expectedExecutionOrder[17] = nodes.get(1);
            expectedExecutionOrder[18] = nodes.get(2);
            expectedExecutionOrder[19] = nodes.get(4);
            
            expectedExecutionOrder[20] = nodes.get(2);
            expectedExecutionOrder[21] = nodes.get(0);
            expectedExecutionOrder[22] = nodes.get(3);
            expectedExecutionOrder[23] = nodes.get(3);
            expectedExecutionOrder[24] = nodes.get(4);
            expectedExecutionOrder[25] = nodes.get(3);
            expectedExecutionOrder[26] = nodes.get(2);
            expectedExecutionOrder[27] = nodes.get(2);
            expectedExecutionOrder[28] = nodes.get(0);
            expectedExecutionOrder[29] = nodes.get(4);
            
            expectedExecutionOrder[30] = nodes.get(3);
            expectedExecutionOrder[31] = nodes.get(1);
            expectedExecutionOrder[32] = nodes.get(1);
            expectedExecutionOrder[33] = nodes.get(3);
            expectedExecutionOrder[34] = nodes.get(4);
            expectedExecutionOrder[35] = nodes.get(0);
            expectedExecutionOrder[36] = nodes.get(2);
            expectedExecutionOrder[37] = nodes.get(2);
            expectedExecutionOrder[38] = nodes.get(3);
            expectedExecutionOrder[39] = nodes.get(4);
           
            expectedExecutionOrder[40] = nodes.get(3);
            expectedExecutionOrder[41] = null;
            expectedExecutionOrder[42] = nodes.get(0);
            expectedExecutionOrder[43] = nodes.get(2);
            expectedExecutionOrder[44] = nodes.get(4);
            expectedExecutionOrder[45] = nodes.get(2);
            expectedExecutionOrder[46] = nodes.get(1);
            expectedExecutionOrder[47] = nodes.get(1);
            expectedExecutionOrder[48] = nodes.get(3);
            expectedExecutionOrder[49] = nodes.get(0);
            
            /// validate order
            for(int i = 0; i < dms.getResult().getExecutionOrder().length; i++)
            {
                assertEquals(expectedExecutionOrder[i], dms.getResult().getExecutionOrder()[i]);
            }
            
            /// validate idle
            assertEquals(1, dms.getResult().getIdleNodes().length);
            
            /// validate late time
            assertEquals(1, dms.getResult().getMissDeadlineNodes().length);
            assertEquals(80, dms.getResult().getTotalLateTime());
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace(System.out);
        }
    }
    
    public void testDMS2()
    {
        System.out.println("TEST CASE DMS 2");
        List<Node> nodes = ReadExcel.ReadExcel("data\\superframe_scheduling.xls");
        int totalTime = 1000;
        int timePerSlot = 10;
        DMS dms = new DMS(nodes, totalTime, timePerSlot);
        try 
        {
            dms.simulateScheduling();
            dms.summaryResult();
            
            ResultData data = dms.getResult();
            
            Node[] expectedExecutionOrder = new Node[totalTime / timePerSlot];
            
            /// validate array length
            assertEquals(expectedExecutionOrder.length, data.getExecutionOrder().length);
            
            /// create data
            expectedExecutionOrder[0] = nodes.get(0);
            expectedExecutionOrder[1] = nodes.get(1);
            expectedExecutionOrder[2] = nodes.get(1);
            expectedExecutionOrder[3] = nodes.get(2);
            expectedExecutionOrder[4] = nodes.get(4);
            expectedExecutionOrder[5] = nodes.get(2);
            expectedExecutionOrder[6] = nodes.get(3);
            expectedExecutionOrder[7] = nodes.get(3);
            expectedExecutionOrder[8] = nodes.get(3);
            expectedExecutionOrder[9] = nodes.get(4);
            
            expectedExecutionOrder[10] = nodes.get(2);
            expectedExecutionOrder[11] = nodes.get(2);
            expectedExecutionOrder[12] = null;
            expectedExecutionOrder[13] = nodes.get(3);
            expectedExecutionOrder[14] = nodes.get(4);
            expectedExecutionOrder[15] = nodes.get(3);
            expectedExecutionOrder[16] = nodes.get(1);
            expectedExecutionOrder[17] = nodes.get(1);
            expectedExecutionOrder[18] = nodes.get(2);
            expectedExecutionOrder[19] = nodes.get(4);
            
            expectedExecutionOrder[20] = nodes.get(2);
            expectedExecutionOrder[21] = nodes.get(3);
            expectedExecutionOrder[22] = null;
            expectedExecutionOrder[23] = nodes.get(3);
            expectedExecutionOrder[24] = nodes.get(4);
            expectedExecutionOrder[25] = nodes.get(0);
            expectedExecutionOrder[26] = nodes.get(2);
            expectedExecutionOrder[27] = nodes.get(2);
            expectedExecutionOrder[28] = nodes.get(3);
            expectedExecutionOrder[29] = nodes.get(4);
            
            expectedExecutionOrder[30] = nodes.get(3);
            expectedExecutionOrder[31] = nodes.get(1);
            expectedExecutionOrder[32] = nodes.get(1);
            expectedExecutionOrder[33] = nodes.get(3);
            expectedExecutionOrder[34] = nodes.get(4);
            expectedExecutionOrder[35] = nodes.get(2);
            expectedExecutionOrder[36] = nodes.get(2);
            expectedExecutionOrder[37] = nodes.get(3);
            expectedExecutionOrder[38] = nodes.get(3);
            expectedExecutionOrder[39] = nodes.get(4);
           
            expectedExecutionOrder[40] = null;
            expectedExecutionOrder[41] = null;
            expectedExecutionOrder[42] = nodes.get(2);
            expectedExecutionOrder[43] = nodes.get(2);
            expectedExecutionOrder[44] = nodes.get(4);
            expectedExecutionOrder[45] = nodes.get(3);
            expectedExecutionOrder[46] = nodes.get(1);
            expectedExecutionOrder[47] = nodes.get(1);
            expectedExecutionOrder[48] = nodes.get(3);
            expectedExecutionOrder[49] = nodes.get(4);
            
            
            /// validate order
            for(int i = 0; i < data.getExecutionOrder().length; i++)
            {
                assertEquals(expectedExecutionOrder[i], data.getExecutionOrder()[i]);
            }
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace(System.out);
        }
    }
}
