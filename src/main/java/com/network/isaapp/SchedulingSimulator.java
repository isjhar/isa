/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.isaapp;

import com.shiddiq.entity.Node;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 *
 * @author Isjhar-pc
 */
public abstract class SchedulingSimulator 
{
    protected final List<Node> nodes;
    protected final List<LateNode> missDeadlineNodes = new ArrayList<>();
    protected final List<IdleSlot> idleSlots = new ArrayList<>();
    protected final Queue<Integer> lateIndexes = new LinkedList<>();
    protected final int totalTime;
    protected final int timePerSlot;
    protected final int totalSlot;
    protected final DataSet dataSet;
    protected int currentTime;
    protected Node currentExecutedNode;
    protected ResultData result;
    
    public SchedulingSimulator(List<Node> nodes, int totalTime, int timePerSlot)
    {
        this.nodes = nodes;
        this.totalTime = totalTime;
        this.timePerSlot = timePerSlot;
        totalSlot = this.totalTime / this.timePerSlot;
        dataSet = createDataSet();
    }
    
    public void simulateScheduling() throws Exception
    {        
        System.out.println("START SIMULATION");
        System.out.println("===============================================================================");
        System.out.println("Total Node : " + nodes.size());
        System.out.println("Total Time : " + totalTime);
        System.out.println("Time Per Slot : " + timePerSlot);
        
        simulating();
        
        System.out.println("");
    }
    protected void resetState()
    {
        missDeadlineNodes.clear();
        idleSlots.clear();
        for(Node node : nodes)
        {
            node.resetExecution();
            node.resetLateState();
            node.resetCompletionTime();
        }
    }
    protected void simulating() throws Exception
    {
        resetState();
        Node[] executionOrder = new Node[totalSlot];
        for(currentTime = 0; currentTime < totalTime; currentTime += timePerSlot)
        {
            syncTimeNodes();
            currentExecutedNode = getHighestPriorityNode();
            if(currentExecutedNode == null)
            {
                idleSlots.add(new IdleSlot(timePerSlot));
                continue;
            }
            executionOrder[currentTime / timePerSlot] = currentExecutedNode;
            currentExecutedNode.execute(timePerSlot);
            checkCurrentExecutedNodeCompleteness();
        }
        syncLateNodes();
        LateNode[] missDeadlineArr = new LateNode[missDeadlineNodes.size()];
        missDeadlineArr = missDeadlineNodes.toArray(missDeadlineArr);
        IdleSlot[] idleSlotArr = new IdleSlot[idleSlots.size()];
        idleSlotArr = idleSlots.toArray(idleSlotArr);
        result = new ResultData(executionOrder, idleSlotArr, missDeadlineArr);
    }
    
    public void summaryResult()
    {
        if(result == null)
        {
            System.out.println("No Result");
            return;
        }
        
        /// title text
        String[] titleRows = new String[nodes.size() + 2];
        titleRows[0] = "Slot Number";
        titleRows[1] = "Executed Node";
        int mostLongTitle = titleRows[1].length();
        for(int i = 2; i < titleRows.length; i++)
        {
            titleRows[i] = nodes.get(i - 2).getName();
            if(mostLongTitle < titleRows[i].length())
                mostLongTitle = titleRows[i].length();
        }
        for(int i = 0; i < titleRows.length; i++)
        {
            int emptySpace = mostLongTitle - titleRows[i].length();
            for(int j = 0; j < emptySpace; j++)
            {
                titleRows[i] += " ";
            }
            titleRows[i] += " : ";
        }
        
        StringBuilder slotNumberBuilder = new StringBuilder();
        slotNumberBuilder.append(titleRows[0]);
        StringBuilder fillBuilder = new StringBuilder();
        fillBuilder.append(titleRows[1]);
        StringBuilder[] nodeTimelineBuilder = new StringBuilder[nodes.size()];
        for(int i = 0; i < nodeTimelineBuilder.length; i++)
        {
            nodeTimelineBuilder[i] = new StringBuilder();
            nodeTimelineBuilder[i].append(titleRows[i + 2]);
        }
        
        int digitAmount = String.valueOf(Math.max(totalSlot, nodes.size())).length();
        String emptySlot = "[";
        for(int k = 0; k < digitAmount; k++)
        {
            emptySlot = emptySlot + " ";
        }
        emptySlot = emptySlot + "]";
                            
        System.out.println("SUMMARY");
        System.out.println("===============================================================================");
        System.out.println("Execution Order : ");
        for(int i = 0; i < result.getExecutionOrder().length; i++)
        {
            /// append slot number to its builder
            String tempStr = String.valueOf((i+1));
            int tempStrLength = tempStr.length();
            for(int j = 0; j < digitAmount - tempStrLength; j++)
            {
                tempStr = " " + tempStr;
            }
//            if(!lateIndexes.isEmpty() && lateIndexes.peek() == i)
//                slotNumberBuilder.append("|");
            slotNumberBuilder.append("[" + tempStr + "]");
            
            /// append fill number to its builder
            tempStr = "";
            if(result.getExecutionOrder()[i] != null)
            {
                tempStr = String.valueOf(dataSet.getNodeIndex(result.getExecutionOrder()[i]));
            }
            tempStrLength = tempStr.length();
            for(int j = 0; j < digitAmount - tempStrLength; j++)
            {
                tempStr = " " + tempStr;
            }
            if(!lateIndexes.isEmpty() && lateIndexes.peek() == i)
            {
//                fillBuilder.append("|");
//                lateIndexes.poll();
            }
            fillBuilder.append("[" + tempStr +"]");
            
            /// fill time line
            for(int j = 0; j < nodeTimelineBuilder.length; j++)
            {
                if(result.getExecutionOrder()[i] != null && nodes.indexOf(result.getExecutionOrder()[i]) == j)
                {
                    nodeTimelineBuilder[j].append("[" + tempStr + "]");
                }
                else
                {
                    nodeTimelineBuilder[j].append(emptySlot);
                }
            }
        }
        System.out.println("===============================================================================");
        System.out.println(slotNumberBuilder.toString());
        System.out.println(fillBuilder.toString());
        for(int i = 0; i < nodeTimelineBuilder.length; i++)
            System.out.println(nodeTimelineBuilder[i].toString());
        System.out.println("===============================================================================");
        System.out.println("Miss deadline : " + result.getMissDeadlineNodes().length + "(" + result.getTotalLateTime()
            + " ms)");
        System.out.println("Idle : " + result.getIdleNodes().length + "(" + result.getTotalIdleTime() + " ms)");
        System.out.println("Total Defect Time : " + result.getTotalDefectTime() + " ms");
        System.out.println("");
    }
    
    public ResultData getResult()
    {
        return result;
    }
    
    private DataSet createDataSet()
    {
        DataSet result = new DataSet();
        for(Node node : nodes)
        {
            if(node.isBeacon())
            {
                if(result.getBeacon() != null)
                {
                    System.out.println("There is more then one beacon in nodes");
                    continue;
                }
                result.setBeacon(node);
            }
            else
            {
                result.getNodes().add(node);
            }
        }
        return result;
    }
    
    private void syncTimeNodes()
    {
        for(Node node : nodes)
        {
            /// node is ready
            if(currentTime >= node.getReadyness())
                node.addCompletionTime(timePerSlot);            
            /// new cycle
            if((currentTime - node.getReadyness()) % node.getPeriod() == 0 )
            {
                /// cycle > 0
                if((currentTime - node.getReadyness()) / node.getPeriod() > 0)
                {
                    /// late
                    if(node.getExecutionTimeTemp() < node.getComputationTime() && !node.isLate())
                    {
                        node.late();
                        lateIndexes.add(currentTime / timePerSlot);
                    }
                    node.resetExecution();
                }
            }
        }
    }
    
    private void syncLateNodes()
    {
        for(Node node : nodes)
        {
            if(node.isLate())
            {
                LateNode lateNode = new LateNode(node, node.getCompletionTime()- node.getDeadline());
                missDeadlineNodes.add(lateNode);
            }
        }
    }
    
    private void checkCurrentExecutedNodeCompleteness()
    {
        /// check node completed
        if(currentExecutedNode.getExecutionTimeTemp() >= currentExecutedNode.getComputationTime())
        {
            if(currentExecutedNode.isLate())
            {
                LateNode lateNode = new LateNode(currentExecutedNode, currentExecutedNode.getCompletionTime() - currentExecutedNode.getDeadline());
                missDeadlineNodes.add(lateNode);
                currentExecutedNode.resetLateState();
            }
            currentExecutedNode.resetCompletionTime();
        }
    }
    
    protected boolean canExecute(Node node)
    {
        if(node == null)
            return false;
        return node.getReadyness() <= currentTime 
                && currentTime < node.getReadyness() + (node.getPeriod() * ((currentTime - node.getReadyness()) / 
                    node.getPeriod())) + node.getDeadline()
                && node.getExecutionTimeTemp() < node.getComputationTime();
    }
    
    protected abstract Node getHighestPriorityNode() throws Exception;
}
