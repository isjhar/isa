/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.isaapp;

import com.shiddiq.entity.Node;
import java.util.ArrayList;
import java.util.List;
import org.jenetics.Chromosome;
import org.jenetics.Genotype;
import org.jenetics.IntegerChromosome;
import org.jenetics.IntegerGene;
import org.jenetics.Mutator;
import org.jenetics.RouletteWheelSelector;
import org.jenetics.SinglePointCrossover;
import org.jenetics.SwapMutator;
import org.jenetics.TruncationSelector;
import org.jenetics.UniformCrossover;
import org.jenetics.engine.Engine;
import org.jenetics.engine.EvolutionResult;
import org.jenetics.engine.limit;

/**
 *
 * @author Isjhar-pc
 */
public class GASIntegerGene extends GAS<IntegerGene>
{
    
    public GASIntegerGene(List<Node> nodes, int totalTime, int timePerSlot) {
        super(nodes, totalTime, timePerSlot);
    }

    @Override
    protected void defineSchedule(Genotype<IntegerGene> genotype)
    {
        schedule.clear();
        for(int i = 0; i < genotype.length(); i++)
        {
            Chromosome<IntegerGene> chromosome = genotype.getChromosome(i);
            for(int j = 0; j < chromosome.length(); j++)
            {
                IntegerGene gene = chromosome.getGene(j);
                schedule.add(dataSet.getNodes().get(gene.getAllele()));
            }
        }
    }
    
    @Override
    protected void defineGenotype()
    {
        final List<IntegerChromosome> chromosomes = new ArrayList<>();
        int beaconSlot = getBeaconSlot();
        for(int i = 0; i < totalSlot - beaconSlot;i++)
            chromosomes.add(IntegerChromosome.of(0, dataSet.getNodes().size() - 1   ));
         factoryGenotype = Genotype.of(chromosomes);
    }

    @Override
    protected void defineEngine() 
    {
        engine = Engine
                .builder(function, factoryGenotype)
                .populationSize(500)
                .alterers(new UniformCrossover<>(0.5, 0.5), new Mutator<>())
                .executor(executor)
                .build();
    }
    
    @Override
    protected void defineEvolutionStream()
    {
        genotypeResult = engine
                .stream() 
                .limit(limit.bySteadyFitness(25))   
//                .limit(limit.byFitnessThreshold(1.0 / 16010.0))
                .peek(statistics)
                .collect(EvolutionResult.toBestGenotype());
    }
    
}
