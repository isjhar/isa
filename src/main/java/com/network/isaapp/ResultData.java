/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.isaapp;

import com.shiddiq.entity.Node;

/**
 *
 * @author Isjhar-pc
 */
public class ResultData 
{
    private LateNode[] missDeadlineNodes;
    private Node[] executionOrder;
    private IdleSlot[] idleNodes;
    private int totalLateTime;
    private int totalIdleTime;
    
    public ResultData(Node[] executionOrder, IdleSlot[] idleNodes, LateNode[] missDeadlineNodes)
    {
        this.executionOrder = executionOrder;
        this.missDeadlineNodes = missDeadlineNodes;
        this.idleNodes = idleNodes;
        
        for(LateNode lateNode : missDeadlineNodes)
        {
            totalLateTime += lateNode.getLateTime();
        }
        
        for(IdleSlot idleNode : idleNodes)
        {
            totalIdleTime += idleNode.getIdleTime();
        }
    }

    public LateNode[] getMissDeadlineNodes() {
        return missDeadlineNodes;
    }

    public void setMissDeadlineNodes(LateNode[] missDeadlineNodes) {
        this.missDeadlineNodes = missDeadlineNodes;
    }

    public Node[] getExecutionOrder() {
        return executionOrder;
    }

    public void setExecutionOrder(Node[] executionOrder) {
        this.executionOrder = executionOrder;
    }
    
    public int getTotalLateTime()
    {
        return totalLateTime;
    }
    
    public int getTotalIdleTime()
    {
        return totalIdleTime;
    }
    
    public int getTotalDefectTime()
    {
        return totalLateTime + totalIdleTime;
    }

    public IdleSlot[] getIdleNodes() {
        return idleNodes;
    }

    public void setIdleNodes(IdleSlot[] idleNodes) {
        this.idleNodes = idleNodes;
    }
}
