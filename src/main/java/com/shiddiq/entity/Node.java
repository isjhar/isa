package com.shiddiq.entity;

public class Node 
{
    private String name;
    private int readyness;
    private int period;
    private int computationTime;
    private int deadline;
    private Boolean isready=false; //default value
    private int periodtemp=0; //default value
    private int executiontimetemp=0; //default value
    private int deadlinetemp=0; //default value
    private int completionTime;
    private Boolean isBeacon;
    private boolean isLate;
    //add by shiddiq for GA parameter
    private int tardiness=0; //default value
 
    public Node(String namein , int readynessin , int periodin , int executiontimein , int deadlinein)
    {
            this.name = namein;
            this.readyness = readynessin;
            this.period = periodin;
            this.computationTime = executiontimein;
            this.deadline = deadlinein;
            this.isBeacon = this.name.contains("Beacon");
    }
		
    public int getPeriodtemp() {
        return periodtemp;
    }
		
    public int setPeriodtemp(int periodtempin) {
        return this.periodtemp=periodtempin;
    }
		
    public int getExecutionTimeTemp() {
        return executiontimetemp;
    }
		
    public int setExecutiontimetemp(int executiontimetempin) {
        return this.executiontimetemp=executiontimetempin;
    }
    
    public void execute(int executedTime)
    {
        this.executiontimetemp += executedTime;
    }
    
    public void resetExecution()
    {
        this.executiontimetemp = 0;
    }
    
    public void resetCompletionTime()
    {
        completionTime = 0;
    }
    
    public void resetLateState()
    {
        isLate = false;
    }
		
    public int getDeadlinetemp() {
        return deadlinetemp;
    }
		
    public int setDeadlinetemp(int deadlinetempin ) {
        return deadlinetemp=deadlinetempin;
    }
		
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
	    
    public int getReadyness() {
        return readyness;
    }
	    
    public void setReadyness(int readyness) {
        this.readyness = readyness;
    }


    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;

    }
	    
    public void decreasePeriodtime(){
        this.period = period-10;
        this.periodtemp = periodtemp+10;
    }
	    
    public int getComputationTime() {
        return computationTime;
    }
	    
    public void setComputationTime(int computationTime) {
        this.computationTime= computationTime;
    }

    public void decreaseExecutiontime()
    {	    	
        this.computationTime = computationTime-10; 
        this.executiontimetemp = executiontimetemp+10;
    }

    public int getDeadline() {
        return deadline;
    }

    public void setDeadline(int deadline) {
        this.deadline = deadline;
    }

    public void  decreaseDeadline(){ 
        this.deadline = deadline-10;
        this.deadlinetemp = deadlinetemp + 10;
    }

    public Boolean getReady() {
        return isready;
    }

    public void setReady(Boolean isreadyin) {
        this.isready = isreadyin;
    }

    //add by shiddiq 
    public int getTardiness() {
        return tardiness;
    }

    public void setTardiness() {
        this.tardiness=computationTime-deadline;
    }

    public String toString(){
         return "{Nodename : "+name
                        + "Readyness :	"+readyness
                        + "Period : "+period
                        + "Execution time : "+computationTime
                        + "Deadline	: "+deadline
                        + "}";

    }

    public Boolean isBeacon() {
        return isBeacon;
    }

    public void setIsBeacon(Boolean isBeacon) {
        this.isBeacon = isBeacon;
    }

    public void late()
    {
        isLate = true;
    }
    
    public boolean isLate()
    {
        return isLate;
    }

    public int getCompletionTime() {
        return completionTime;
    }
    
    public void addCompletionTime(int addValue)
    {
        completionTime += addValue;
    }
}    


